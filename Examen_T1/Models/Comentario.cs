﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public int Postid { get; set; }
        public string Detalle { get; set; }
        public DateTime Fecha { get; set; }
    }
}
