﻿using Examen_T1.DB.Mapping;
using Examen_T1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.DB
{
    public class BlogContext: DbContext
    {
        public DbSet<Post> posts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public BlogContext(DbContextOptions<BlogContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new BlogMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());
        }
    }
}
